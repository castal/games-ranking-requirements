#Games Ranking

Um grupo de amigos costuma praticar diversas modalidades de jogos. 
Em suas conversas o assunto sempre é: quem é melhor, quem ganha mais partidas. 
Foi aí que um deles teve a idéia de preparar um sistema que gravasse um Ranking dos jogos. 
Esse ranking mostraria o jogador, as vitórias e as partidas que esse jogador participou. Exemplo:

```
+-------------------------------+
| Jogador | Vitórias | Partidas |
|---------|----------|----------|
| José    | 30       | 30       | 
| Carlos  | 20       | 25       | 
| Camila  | 15       | 35       | 
+-------------------------------+
```

##Funcionalidades

- Dado o contexto acima, identifique e especifique ao menos 3 requisitos que atendam a proposta do sistema.


##Requisitos

- Documento de visão: elaboração do documento de visão do projeto;
- Especificação de requisitos: elaboração do documento de especificação de requisitos;
- Protótipos: elaboração dos protótipos das telas do sistema;
- Modelo de dados: elaboração do modelo físico de dados.


##Observações:

- Para elaboração do documento de visão e especificação de requisitos, poderão ser utilizado editores de texto padrão (Microsoft Word ou LibreOffice) ou ferramentas case (Enterprise Architect);
- Os protótipos elaborados poderão ser não-funcionais e de baixa fidelidade. Poderão ser utilizadas ferramentas como Balsamiq ou Axure.
- A ferramenta utilizada para modelagem dos dados é de livre escolha do candidato. Apenas a imagem do modelo deverá ser submetida.

##Avaliação 

Serão avaliado os seguintes pontos:

- Visão geral do problema e seus aspectos;
- Nível de detalhamento da especificação;
- Tempo de entrega do desafio.
